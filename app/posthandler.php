<?php

use VhostManager\ProxyPass;
use VhostManager\Vhost;

if (!empty($_POST)) {
    switch($_POST['action'])
    {
        case 'create':
            create([
                'serverName',
                'port',
                'serverAlias',
                'serverAdmin',
                'documentRoot',
                'phpVersion',
                'useHTTPS',
                'useProxy',
                'proxyRedirectFrom',
                'proxyRedirectTo',
                'reverseProxyRedirectFrom',
                'reverseProxyRedirectTo',
                'action',
            ]);
            break;
        case 'disable':
            disable(['name']);
            break;
        case 'enable':
            enable(['name']);
            break;
        case 'delete':
            delete(['name']);
            break;
        default:
            unprocessable();
            break;
    }
}

function create($neededKeys)
{
    if (array_diff_key(array_flip($neededKeys), $_POST)) {
        unprocessable();
    }
    
    $useHttps = filter_var($_POST['useHTTPS'], FILTER_VALIDATE_BOOLEAN);
    $useProxy = filter_var($_POST['useProxy'], FILTER_VALIDATE_BOOLEAN);
    $vhost = new Vhost();
    
    $vhost->setDocumentRoot($_POST['documentRoot'])
        ->setPort($_POST['port'])
        ->setServerAdmin($_POST['serverAdmin'])
        ->setServerAlias($_POST['serverAlias'])
        ->setServerName($_POST['serverName'])
        ->setPhpVersion($_POST['phpVersion']);
    
    if ($useProxy) {
        $proxyPass = new ProxyPass();
        $proxyPass->useHttps($useHttps)
            ->setRedirectFrom($_POST['proxyRedirectFrom'])
            ->setRedirectTo($_POST['proxyRedirectTo'])
            ->setReverseRedirectFrom($_POST['reverseProxyRedirectFrom'])
            ->setReverseRedirectTo($_POST['reverseProxyRedirectTo']);
        $vhost->setProxyPass($proxyPass);
    }
    
    if (!$vhost->save(true) || !$vhost->activate(true)) {
        internalError();
    } else {
        header("Refresh:0");
    }
}

function enable($neededKeys)
{
    if (array_diff_key(array_flip($neededKeys), $_POST)) {
        unprocessable();
    }
    
    if (! Vhost::enable(cutConfSuffix($_POST['name'])) === true) {
        internalError();
    } else {
        header("Refresh:0");
    }
}

function disable($neededKeys)
{
    if (array_diff_key(array_flip($neededKeys), $_POST)) {
        unprocessable();
    }
    
    if (! Vhost::disable(cutConfSuffix($_POST['name'])) === true) {
        internalError();
    } else {
        header("Refresh:0");
    }
}

function delete($neededKeys)
{
    if (array_diff_key(array_flip($neededKeys), $_POST)) {
        unprocessable();
    }
    
    if (! Vhost::delete(cutConfSuffix($_POST['name']), true) === true) {
        internalError();
    } else {
        header("Refresh:0");
    }
}

function internalError()
{
    header("HTTP/1.0 500 Internal Server Error");
    exit();
}

function unprocessable()
{
    header("HTTP/1.0 422 Unprocessable Entity");
    exit();
}

function cutConfSuffix(String $url)
{
    if (preg_match('/^.*\.conf$/', $url)) {
        return substr($url, 0, -5);
    } else {
        return $url;
    }
}
