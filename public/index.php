<?php

use VhostManager\Vhost;

include_once('../vendor/autoload.php');
include_once ('../app/posthandler.php');

$enabled = Vhost::getEnabledVhosts();
$disabled = Vhost::getDisabledVhosts();
?>

<html lang="en">
<head>
    <?php include_once '../resources/head.html' ?>
    <title>Vhost Creator</title>
</head>
<body class="bg-darker">
<div class="container d-flex justify-content-around text-light">
    <div class="col-lg-6 mx-4 bg-darker main-form card border-light">
        <?php include_once '../resources/disabled-vhosts.php' ?>
    </div>
    
    <div class="col-lg-5 mx-4 bg-darker main-form card border-light">
        <?php include_once '../resources/vhost-form.html' ?>
    </div>
    
    <div class="col-lg-6 mx-4 bg-darker main-form card border-light">
        <?php include_once '../resources/enabled-vhosts.php' ?>
    </div>
</div>

</body>
</html>
