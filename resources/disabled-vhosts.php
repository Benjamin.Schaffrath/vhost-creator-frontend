<?php

$vhosts = "";

foreach ($disabled as $host) {
    $vhosts .= sprintf('
        <tr>
          <td>%s</td>
          <td class="row">
            <form method="post">
                <input hidden class="form-control" name="name" value="%s">
                <input hidden class="form-control" name="action" value="enable">
                <button type="submit" class="btn btn-primary btn-sml mx-1">Enable</button>
            </form>
            
            <form method="post">
                <input hidden class="form-control" name="name" value="%s">
                <input hidden class="form-control" name="action" value="delete">
                <button type="submit" class="btn btn-danger btn-sml mx-1">Delete</button>
            </form>
          </td>
        </tr>
    ',
    cutConfSuffix($host),
    $host,
    $host
    );
}

$html = sprintf('
    <div class="card-title d-flex justify-content-center my-2">
        <h4>Disabled VHosts</h4>
    </div>
    <table class="table table-sm table-striped borderless table-hover table-dark">
        <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Options</th>
            </tr>
        </thead>
        <tbody>
            %s
        </tbody>
    </table>',
    $vhosts
);


echo($html);
