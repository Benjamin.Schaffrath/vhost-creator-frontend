$(document).ready(function () {
    function initialize() {
        $('#serverName').val(".docker.jtl-software.de");
        $('#port').val(80);
        $('#serverAlias').val("www.docker.jtl-software.de");
        $('#serverAdmin').val("benjamin.schaffrath@jtl-software.com");
        $('#documentRoot').val("");
        $('#useHTTPS').prop('checked', false);
        $('#useProxy').prop('checked', true);
        let useProxy = $('#proxySettings');
        useProxy.prop('checked', true);
        useProxy.addClass("collapse show");
        $('#proxyRedirectFrom').val("/");
        $('#proxyRedirectTo').val("127.0.0.1:8088");
        $('#reverseProxyRedirectFrom').val("/");
        $('#reverseProxyRedirectTo').val("127.0.0.1:8088");
    }

    $('#btnClear').on("click", function (event) {
        let proxySettings = $('#proxySettings');
        if (proxySettings.hasClass('show')) {
            proxySettings.removeClass('show')
        }
        if (!proxySettings.hasClass('collapse')) {
            proxySettings.addClass('collapse')
        }

        $('#vhost-form')[0].reset()
    });

    $('#btnCreate').on("click", function (event) {
        event.preventDefault();

        let serverName = $('#serverName').val();
        let port = $('#port').val();
        let serverAlias = $('#serverAlias').val();
        let serverAdmin = $('#serverAdmin').val();
        let documentRoot = $('#documentRoot').val();
        let useHTTPS = $('#useHTTPS').prop("checked");
        let useProxy = $('#useProxy').prop("checked");
        let proxyRedirectFrom = $('#proxyRedirectFrom').val();
        let proxyRedirectTo = $('#proxyRedirectTo').val();
        let reverseProxyRedirectFrom = $('#reverseProxyRedirectFrom').val();
        let reverseProxyRedirectTo = $('#reverseProxyRedirectTo').val();
        let phpVersion = document.getElementById('phpVersion').value;

        if (serverName === '' || port === '' || (!useProxy && documentRoot === '') || (useProxy && (proxyRedirectFrom === '' || proxyRedirectTo === ''))) {
            alert("Not all necessary fields are filled!");

            return;
        }

        let parameters = {
            'serverName': serverName,
            'port': port,
            'serverAlias': serverAlias,
            'serverAdmin': serverAdmin,
            'documentRoot': documentRoot,
            'phpVersion': phpVersion,
            'useHTTPS': useHTTPS,
            'useProxy': useProxy,
            'proxyRedirectFrom': proxyRedirectFrom,
            'proxyRedirectTo': proxyRedirectTo,
            'reverseProxyRedirectFrom': reverseProxyRedirectFrom,
            'reverseProxyRedirectTo': reverseProxyRedirectTo,
            'action': 'create'
        };
        $.post(
            "index.php", parameters,
            function (data) {
                alert('Successfully created & activated the VHost!');
                location.reload();
            }
        ).fail(function () {
            alert('Error while creating VHost!');
        });
    });
});
